module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    project: {
        src: "src",
        build: "build",
        css: ["<%= project.src %>/scss/AMUSubtitlesPlayer.scss"],
        js: ["<%= project.src %>/js/*.js"]
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd hh:MM:ss") %> */\n'
      },
      build: {
        src: '<%= project.src %>/js/AMUSubtitlesPlayer.js',
        dest: '<%= project.build %>/js/AMUSubtitlesPlayer.min.js'
      }
    },
    sass: {
        dev: {
            options: {
                style: "expanded",
                compass: true
            },
            files: {
                "<%= project.src %>/css/style.css" : "<%= project.css %>"
            }
        },
        dist: {
            options: {
                style: "compressed",
                compass: true
            },
            files: {
                "<%= project.build %>/css/style.min.css" : "<%= project.css %>"
            }
        }
    },
    watch: {
      css: {
        files: ['src/scss/AMUSubtitlesPlayer.scss'],
        tasks: ['sass:dist', "sass:dev"]
      },
      js: {
        files: ['src/js/AMUSubtitlesPlayer.js'],
        tasks: ['uglify:build']
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  
  // "Watch file changes" task.
  grunt.loadNpmTasks('grunt-contrib-watch');
  
  // SASS
  grunt.loadNpmTasks('grunt-contrib-sass');

  // Default task(s).
  grunt.registerTask('default', ['watch']);

};