function AMUSubtitlesPlayer(DOMVideoObject){
    var that                 = this;
    var video                = null;
    var subtitles            = null;
    var subtitlesControler   = null;
    var isFullScreen         = false;
    var playerContainer      = null;
    var controls             = null;
    var timeoutValue         = null;
    var controlsHideDuration = 5000;
    var playbackSeekerWidth  = 0;
    var playbackSeeker       = null;
    var seekerHorizontalPos  = 0;
    var dragHorizontalRange  = [];
    
    var styleClasses        = {};
    styleClasses.container  = "AMUSubtitlesPlayer";
    styleClasses.video      = styleClasses.container+"-video";
    styleClasses.subtitles  = styleClasses.container+"-subtitles";
    styleClasses.controls   = styleClasses.container+"-controls";
    styleClasses.fullscreen = styleClasses.container+"-fullscreen";
    
    function parseSubtitles(subtitlesText){
        var subtitleLines   = null;
        var subtitleObject  = null;
        
        function getStartTime(inputText){
            if (inputText && typeof(inputText) === "string") {
                var timeNumbers = inputText.substr(0, 12).split(":");
                for (var t=0; t<timeNumbers.length; t++) {
                    timeNumbers[t]  = parseFloat(timeNumbers[t]);
                    if (isNaN(timeNumbers[t])) {
                        return null;
                    }
                }
                timeNumbers[0]  = timeNumbers[0]*3600;
                timeNumbers[1]  = timeNumbers[1]*60;
                return timeNumbers.reduce(function(pv, cv) { return pv + cv; }, 0);
            }
            return null;
        }
        function getEndTime(inputText){
            if (inputText && typeof(inputText) === "string") {
                var timeNumbers = inputText.substr(13, 12).split(":");
                for (var t=0; t<timeNumbers.length; t++) {
                    timeNumbers[t]  = parseFloat(timeNumbers[t]);
                    if (isNaN(timeNumbers[t])) {
                        return null;
                    }
                }
                timeNumbers[0]  = timeNumbers[0]*3600;
                timeNumbers[1]  = timeNumbers[1]*60;
                return timeNumbers.reduce(function(pv, cv) { return pv + cv; }, 0);
            }
            return null;
        }
        
        if (subtitlesText && typeof(subtitlesText) === "string" && subtitlesText !== "") {
            subtitleLines   = subtitlesText.split("\n");
            if (subtitleLines !== null && subtitleLines.length > 0) {
                subtitles   = [];
                for (var sl=0; sl<subtitleLines.length; sl++) {
                    if (subtitleLines[sl]) {
                        subtitleObject  = {
                            begin:  getStartTime(subtitleLines[sl]),
                            end:    getEndTime(subtitleLines[sl]),
                            text:   subtitleLines[sl].substr(26, subtitleLines[sl].length)
                        };
                        if (subtitleObject.begin !== null && subtitleObject.end !== null) {
                            subtitles.push(subtitleObject);
                        }
                    }
                }
            }
        }
    }
    
    function updateSubtitleDisplay(){
        var subtitle    = getCurrentSubtitle();
        if (subtitle) {
            setSubtitleText(subtitle.text);
        } else {
            setSubtitleText("");
        }
    }
    
    function setPlayer(DOMVideoObject){
        if (DOMVideoObject !== undefined && DOMVideoObject !== null && document.createElement('video').canPlayType) {
            if (!window.hasOwnProperty("HTMLVideoElement") || !(DOMVideoObject instanceof HTMLVideoElement)) {
                return null;
            }
            video  = DOMVideoObject;
            if (video !== null) {
                video.controls     = false;
                video.className    = styleClasses.video;
            }
        }
    }
    
    function updatePlaybackProgressBar(){
        var handlePosition  = 0;
        if (video && playbackSeeker) {
            handlePosition  = playbackSeekerWidth*(video.currentTime / video.duration);
            playbackSeeker.handle.style.left    = handlePosition+"px";
            playbackSeeker.progress.style.width = handlePosition+"px";
        }
    }
    
    Node.prototype.AMUaddStyleClass = function(styleClass){
        if (styleClass && typeof(styleClass) === "string" && !this.AMUhasStyleClass(styleClass)) {
            var classes = this.className;
            if (classes) {
                classes = classes.split(" ");
                classes.push(styleClass);
            } else {
                classes = [styleClass];
            }
            this.className  = classes.join(" ");
        }
    };
    Node.prototype.AMUhasStyleClass = function(styleClass){
        if (styleClass && typeof(styleClass) === "string") {
            var classes = this.className;
            if (classes) {
                classes = classes.split(" ");
                for (var c=0; c<classes.length; c++){
                    if (classes[c] === styleClass) {
                        return true;
                    }
                }
            }
        }
        return false;
    };
    Node.prototype.AMUremoveStyleClass  = function(styleClass){
        if (styleClass && typeof(styleClass) === "string" && this.AMUhasStyleClass(styleClass)) {
            var classes = this.className.split(" ");
            classes = classes.filter(function(elem){ return elem !== styleClass; });
            this.className  = classes.join(" ");
        }
    };
    Node.prototype.AMUaddEvent  = function(eventName, functionReference){
        if (this.attachEvent) {
            return this.attachEvent('on' + eventName, functionReference);
        } else {
            return this.addEventListener(eventName, functionReference, false);
        }
    };
    
    
    function rearrangeDOM(){
        var faClasses  = {
            buttonPlay:         " fa fa-play",
            buttonPause:        " fa fa-pause",
            buttonStop:         " fa fa-stop",
            buttonFullScreen:   " fa fa-arrows-alt"
        };

        if (playerContainer) {
            return null;
        }
        if (hasVideo()) {
            playerContainer = document.createElement("div");
            playerContainer.className   = styleClasses.container;
            
            document.body.insertBefore(playerContainer, video);
            playerContainer.appendChild(video);
            
            controls    = {
                container:          document.createElement("div"),
                buttonPlay:         document.createElement("button"),
                buttonPause:        document.createElement("button"),
                buttonStop:         document.createElement("button"),
                seeker:             (function(){
                    var seekerContainer = document.createElement("div");
                    var seekerBar       = document.createElement("div");
                    var seekerProgress  = document.createElement("div");
                    var seekerHandle    = document.createElement("div");
                    
                    seekerBar.className         = styleClasses.controls + "-seekerBar";
                    seekerProgress.className    = styleClasses.controls + "-seekerProgress";
                    seekerBar.appendChild(seekerProgress);
                    seekerContainer.appendChild(seekerBar);
                    
                    seekerHandle.className      = styleClasses.controls + "-seekerHandle";
                    seekerContainer.appendChild(seekerHandle);
                    
                    return {
                        container:  seekerContainer,
                        bar:        seekerBar,
                        progress:   seekerProgress,
                        handle:     seekerHandle
                    };
                })(),
                buttonFullScreen:   document.createElement("button")
            };
            
            controls.container.className  = styleClasses.controls;
            
            for (var key in controls) {
                if (key !== "container" && !controls[key].hasOwnProperty("progress")) {
                    controls[key].className = styleClasses.controls + "-" + key;
                    if (faClasses.hasOwnProperty(key)) {
                        controls[key].className += faClasses[key];
                    }
                    controls.container.appendChild(controls[key]);
                    
                } else if (controls[key].hasOwnProperty("progress")) {
                    playbackSeeker  = controls[key];
                    
                    controls[key].container.className = styleClasses.controls + "-" + key;
                    controls.container.appendChild(controls[key].container);
                }
            }
            playerContainer.appendChild(controls.container);
        }
    }
    
    function attachEvents(){
        if (video) {
            video.AMUaddEvent("playing", function(){
                updateSubtitleDisplay();
            });
            video.AMUaddEvent("timeupdate", function(){
                updateSubtitleDisplay();
                updatePlaybackProgressBar();
            });
            video.AMUaddEvent("click", function(){
                if (video.paused) {
                    that.play();
                } else {
                    that.pause();
                }
            });
            video.AMUaddEvent("dblclick", function(){
                that.toggleFullscreen();
            });
            video.AMUaddEvent("play", function(){
                that.play();
            });
            video.AMUaddEvent("ended", function(){
                that.pause();
            });
        }
        if (controls) {
            controls.buttonPlay.AMUaddEvent("click", function(){
                that.play();
            });
            controls.buttonPause.AMUaddEvent("click", function(){
                that.pause();
            });
            controls.buttonStop.AMUaddEvent("click", function(){
                that.stop();
            });
            controls.buttonFullScreen.AMUaddEvent("click", function(){
                that.toggleFullscreen();
            });
        }
        if (playbackSeeker) {
            playbackSeeker.updateDimensions   = function(){
                playbackSeekerWidth = playbackSeeker.bar.clientWidth-playbackSeeker.handle.clientWidth;
                seekerHorizontalPos = playbackSeeker.bar.getBoundingClientRect().left;
                dragHorizontalRange = [seekerHorizontalPos, seekerHorizontalPos+playbackSeekerWidth];
            };
            playbackSeeker.getPlaybackTimeByMousePosition = function(e){
                var mouseHorizontalPosition = e.clientX;
                if (mouseHorizontalPosition >= dragHorizontalRange[0] && mouseHorizontalPosition <= dragHorizontalRange[1]) {
                    var handlePosition      = mouseHorizontalPosition-seekerHorizontalPos;
                    var playbackProgress    = handlePosition/playbackSeekerWidth;
                    return playbackProgress*video.duration;
                }
                return null;
            };

            playbackSeeker.updateDimensions();

            playbackSeeker.handle.AMUaddEvent("mousedown", function(e){
                e.target.AMUaddStyleClass("is-being-dragged");
                playbackSeeker.updateDimensions();
            });
            playbackSeeker.bar.AMUaddEvent("click", function(e){
                playbackSeeker.updateDimensions();
                that.gotoTime(playbackSeeker.getPlaybackTimeByMousePosition(e));
            });
            document.AMUaddEvent("mouseup", function(){
                if (playbackSeeker.handle.AMUhasStyleClass("is-being-dragged")) {
                    playbackSeeker.handle.AMUremoveStyleClass("is-being-dragged");
                }
            });
            document.AMUaddEvent("mousemove", function(e){
                if (playbackSeeker.handle.AMUhasStyleClass("is-being-dragged")) {
                    that.gotoTime(playbackSeeker.getPlaybackTimeByMousePosition(e));
                }
                if (controls && controls.container.AMUhasStyleClass("is-idle")) {
                    controls.container.AMUremoveStyleClass("is-idle");
                }
                if (timeoutValue) {
                    clearTimeout(timeoutValue);
                }
                timeoutValue    = setTimeout(function(){
                    console.log(controlsHideDuration);
                    if (controls) {
                        controls.container.AMUaddStyleClass("is-idle");
                    }
                }, controlsHideDuration);
            });
        }
    }
    
    function getCurrentSubtitle(){
        var filteredArray   = [];
        if (video && subtitles) {
            filteredArray   = subtitles.filter(function(subtitle){
                if (subtitle && subtitle.begin <= video.currentTime && subtitle.end >= video.currentTime) {
                    return true;
                }
                return false;
            });
            if (filteredArray.length > 0) {
                return filteredArray[0];
            }
        }
        return null;
    }
    
    function hasVideo(){
        if (video !== null) {
            return true;
        }
        return false;
    }
    
    function createSubtitlesControler(){
        if (subtitlesControler) {
            return subtitlesControler;
        }else if (hasVideo()) {
            subtitlesControler = (function(){
                var container       = document.createElement("div");
                var textNode        = document.createTextNode("");
                
                this.setText    = function(text){
                    textNode.textContent   = text;
                };
                this.clearText  = function(){
                    textNode.textContent   = "";
                };
                
                container.className = styleClasses.subtitles;
                container.appendChild(textNode);
                
                container.onclick   = function(){
                    if (video.paused) {
                        video.play();
                    } else {
                        video.pause();
                    }
                };
                
                playerContainer.insertBefore(container, video);
                
                document.onmozfullscreenchange      = function(e){
                    onFullScreenToggle(e);
                };
                document.onfullscreenchange         = function(e){
                    onFullScreenToggle(e);
                };
                document.onwebkitfullscreenchange   = function(e){
                    onFullScreenToggle(e);
                };
                document.onMSFullscreenChange       = function(e){
                    onFullScreenToggle(e);
                };
                
                return this;
            })();
            return subtitlesControler;
        }
    }
    
    function onFullScreenToggle(){
        var playerStyleClasses      = playerContainer.className ? playerContainer.className.split(" ") : [];
        var classIndex              = -1;
        
        if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {
            isFullScreen    = false;
            classIndex  = playerStyleClasses.indexOf(styleClasses.fullscreen);
            if (classIndex >= 0) {
                playerStyleClasses.splice(classIndex, 1);
            }
        } else {
            isFullScreen    = true;
            classIndex  = playerStyleClasses.indexOf(styleClasses.fullscreen);
            if (classIndex < 0) {
                playerStyleClasses.push(styleClasses.fullscreen);
            }
        }
        playerContainer.className   = playerStyleClasses.join(" ");
        if (isFullScreen) {
            video.style.marginTop   = -1*(video.clientHeight/2)+"px";
        } else {
            video.style.marginTop   = "0px";
        }
        if (playbackSeeker) {
            playbackSeekerWidth = playbackSeeker.bar.clientWidth-playbackSeeker.handle.clientWidth;
            seekerHorizontalPos = playbackSeeker.bar.getBoundingClientRect().left;
            dragHorizontalRange = [seekerHorizontalPos, seekerHorizontalPos+playbackSeekerWidth];
            updatePlaybackProgressBar();
        }
    }
    
    function setSubtitleText(text){
        if (!subtitlesControler) {
            subtitlesControler = createSubtitlesControler();
        }
        if (subtitlesControler) {
            subtitlesControler.setText(text);
        }
    }
    
    /* Metody publiczne */
    this.play   = function(){
        // Metoda uruchamia odtwarzanie filmu
        if (video && controls) {
            if (video.paused) {
                video.play();
            }
            controls.buttonPlay.style.display   = "none";
            controls.buttonPause.style.display  = "inline-block";
        }
    };
    
    this.pause  = function(){
        // Metoda wstrzymuje odtwarzanie filmu
        if (video && controls) {
            if (!video.paused) {
                video.pause();
            }
            controls.buttonPlay.style.display   = "inline-block";
            controls.buttonPause.style.display  = "none";
        }
    };
    
    this.stop   = function(){
        // Metoda zatrzymuje odtwarzanie filmu i przesuwa wskaźnik czasu na jego początek
        that.pause();
        video.currentTime   = 0;
        updatePlaybackProgressBar();
    };
    
    this.gotoTime   = function(playbackTime){
        // Metoda przesuwa wskaźnik czasu odtwarzania do żądanej sekundy
        if (video) {
            if (!isNaN(playbackTime) && playbackTime >= 0 && playbackTime <= video.duration) {
                video.currentTime   = playbackTime;
                updatePlaybackProgressBar();
            }
        }
    };
    
    this.toggleFullscreen   = function(){
        // Metoda przełącza widok do/z trybu pełnego ekranu
        if (!isFullScreen) {
            if (document.documentElement.requestFullscreen) {
                document.documentElement.requestFullscreen();
            } else if (document.documentElement.msRequestFullscreen) {
                document.documentElement.msRequestFullscreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullscreen) {
                document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
        }
    };
    
    this.loadSubtitles  = function(subtitlesURL){
        // Metoda wczytuje napisy z pliku
        var xhr = null;
        if (subtitlesURL && typeof(subtitlesURL) === "string" && subtitlesURL !== "") {
            if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();
            } else {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xhr.onreadystatechange  = function(){
                if (xhr.readyState === 4 && xhr.status === 200) {
                    parseSubtitles(xhr.responseText);
                    createSubtitlesControler();
                }
            };
            xhr.open("GET", subtitlesURL, true);
            xhr.send();
        }
    };
    
    setPlayer(DOMVideoObject);
    rearrangeDOM();
    attachEvents();
    
    if (hasVideo()) {
        return this;
    }
    return null;
}